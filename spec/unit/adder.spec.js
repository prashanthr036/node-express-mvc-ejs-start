var adder = require('../../services/adder');

describe("adder tests ", function() {
    var _numberA;
    var _numberB;

    it("should return numberA added with numberB", function() {
        _numberA = 6;
        _numberB = 2;
        var result = adder.add(_numberA, _numberB);

        expect(result).toEqual(8);
    });
    it("should return numberA for numberB equals 0", function() {
        _numberA = 2;
        _numberB = 0;
        var result = adder.add(_numberA, _numberB);

        expect(result).toEqual(2);
    });

});