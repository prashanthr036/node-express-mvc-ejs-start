/** 
*  User model
*  Describes the characteristics of each attribute in a user resource.
*
* @author Prashanth Reddy <S534571@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  name: { type: String, required: true, default: 'Prashanth' },
  city: { type: String, required: true, default: 'Maryville' },
  state: { type: String, required: true, default: 'MO' },
  country: { type: String, required: true, default: 'USA' }
})

module.exports = mongoose.model('User', UserSchema)