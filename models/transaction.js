/** 
*  Transaction model
*  Describes the characteristics of each attribute in a transaction resource.
*
* @author Chaitra Vemula <S534732@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const TransactionSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  Sendername: { type: String, required: true},
  Transaction_amount: { type: Number, required: true },
  Transaction_type: { type: String, required: true}
  
})

module.exports = mongoose.model('Transaction', TransactionSchema)